var mapper = (function() {
 	"use strict";
 	return {
 		mapPermissions : (function(permissions) {
 			var data = {};
 			var obj  = angular.copy(permissions,obj);
 			angular.forEach(obj, function(value, key){
 				var temp = [];
 				temp = Object.keys(value).map(function(k) { return value[k] });	
 				eval("data." + key + "=temp");
 			});
 			data.basic = ["all"];
 			return data;
		})
 	};
 }());