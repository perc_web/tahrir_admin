/***
 Metronic AngularJS App Main Script
 ***/

/* Metronic App */
var MetronicApp = angular.module("MetronicApp", [
    "ui.router",
    "ui.bootstrap",
    "oc.lazyLoad",
    "ngSanitize",
    "as.sortable",
    "ngFileUpload",
    "uiCropper",
    "ngMaterial",
    "textAngular"
]);

/* Configure ocLazyLoader(refer: https://github.com/ocombe/ocLazyLoad) */
MetronicApp.config(['$ocLazyLoadProvider', function ($ocLazyLoadProvider) {
    $ocLazyLoadProvider.config({
        //
    });
}]);

/********************************************
 BEGIN: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/
/**
 `$controller` will no longer look for controllers on `window`.
 The old behavior of looking on `window` for controllers was originally intended
 for use in examples, demos, and toy apps. We found that allowing global controller
 functions encouraged poor practices, so we resolved to disable this behavior by
 default.

 To migrate, register your controllers with modules rather than exposing them
 as globals:

 Before:

 ```javascript
 function MyController() {
  // ...
}
 ```

 After:

 ```javascript
 angular.module('myApp', []).controller('MyController', [function() {
  // ...
}]);

 Although it's not recommended, you can re-enable the old behavior like this:

 ```javascript
 angular.module('myModule').config(['$controllerProvider', function($controllerProvider) {
  // this option might be handy for migrating old apps, but please don't use it
  // in new ones!
  $controllerProvider.allowGlobals();
}]);
 **/

//AngularJS v1.3.x workaround for old style controller declarition in HTML
MetronicApp.config(['$controllerProvider', function ($controllerProvider) {
    // this option might be handy for migrating old apps, but please don't use it
    // in new ones!
    $controllerProvider.allowGlobals();
}]);

/********************************************
 END: BREAKING CHANGE in AngularJS v1.3.x:
 *********************************************/

/* Setup global settings */
MetronicApp.factory('settings', ['$rootScope', function ($rootScope) {
    // supported languages
    var settings = {
        layout: {
            pageSidebarClosed: true, // sidebar menu state
            pageContentWhite: true, // set page content layout
            pageBodySolid: false, // solid body color state
            pageAutoScrollOnLoad: 1000 // auto scroll to top on page load
        },
        assetsPath: 'assets',
        globalPath: 'assets/global',
        layoutPath: 'assets/layouts/layout4',
    };

    $rootScope.settings = settings;

    return settings;
}]);

/* Setup App Main Controller */
MetronicApp.controller('AppController', ['$scope', '$rootScope', function ($scope, $rootScope) {
    $scope.$on('$viewContentLoaded', function () {
        App.initComponents(); // init core components
        //Layout.init(); //  Init entire layout(header, footer, sidebar, etc) on page load if the partials included in server side instead of loading with ng-include directive 
    });
}]);

/***
 Layout Partials.
 By default the partials are loaded through AngularJS ng-include directive. In case they loaded in server side(e.g: PHP include function) then below partial
 initialization can be disabled and Layout.init() should be called on page load complete as explained above.
 ***/

/* Setup Layout Part - Header */
MetronicApp.controller('HeaderController', ['$scope', '$rootScope', 'localStorageService', function ($scope, $rootScope, localStorageService) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initHeader(); // init header
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('SidebarController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initSidebar(); // init sidebar
    });
}]);

/* Setup Layout Part - Sidebar */
MetronicApp.controller('PageHeadController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Demo.init(); // init theme panel
    });
}]);

/* Setup Layout Part - Quick Sidebar */
// MetronicApp.controller('QuickSidebarController', ['$scope', function ($scope) {
//     $scope.$on('$includeContentLoaded', function () {
//         setTimeout(function () {
//             QuickSidebar.init(); // init quick sidebar
//         }, 2000)
//     });
// }]);

/* Setup Layout Part - Theme Panel */
// MetronicApp.controller('ThemePanelController', ['$scope', function ($scope) {
//     $scope.$on('$includeContentLoaded', function () {
//         Demo.init(); // init theme panel
//     });
// }]);

/* Setup Layout Part - Footer */
MetronicApp.controller('FooterController', ['$scope', function ($scope) {
    $scope.$on('$includeContentLoaded', function () {
        Layout.initFooter(); // init footer
    });
}]);

/* Setup Rounting For All Pages */
MetronicApp.config(['$stateProvider', '$urlRouterProvider', function ($stateProvider, $urlRouterProvider) {
    // Redirect any unmatched url
    $urlRouterProvider.otherwise("/trips");

    $stateProvider

    // Dashboard
        // .state('dashboard', {
        //     url: "/dashboard",
        //     templateUrl: "views/partials/dashboard.html",
        //     data: {pageTitle: 'Home'},
        //     controller: "DashboardController",
        //     resolve: {
        //         deps: ['$ocLazyLoad', function ($ocLazyLoad) {
        //             return $ocLazyLoad.load({
        //                 name: 'MetronicApp',
        //                 insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
        //                 files: [
        //                     'assets/global/plugins/morris/morris.css',
        //                     'assets/global/plugins/morris/morris.min.js',
        //                     'assets/global/plugins/morris/raphael-min.js',
        //                     'assets/global/plugins/jquery.sparkline.min.js',
        //                     'assets/pages/scripts/dashboard.min.js',
        //                     'js/controllers/DashboardController.js'
        //                 ]
        //             });
        //         }]
        //     }
        // })

    // Login
        .state('login', {
            url: "/login",
            templateUrl: "views/partials/login.html",
            data: {pageTitle: 'Login'},
            controller: "LoginController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'assets/global/plugins/select2/css/select2.min.css',
                            'assets/pages/css/login-4.min.css',
                            'assets/global/plugins/select2/js/select2.full.min.js',
                            'js/controllers/LoginController.js'
                        ]
                    });
                }]
            }
        })
    // Logout
        .state('logout', {
            url: "/logout",
            templateUrl: "views/partials/login.html",
            data: {pageTitle: 'Logout'},
            controller: "LogoutController",
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
                        files: [
                            'js/controllers/LogoutController.js'
                        ]
                    });
                }]
            }
        })              
        // AngularJS plugins
        .state('profile', {
            url: "/profile",
            templateUrl: "views/partials/profile.html",
            controller: "ProfileController",
            data: {pageTitle: 'Profile'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                            'assets/global/plugins/datatables/datatables.min.css',
                            'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                            'assets/global/plugins/datatables/datatables.all.min.js',
                            <!-- BEGIN PAGE LEVEL PLUGINS -->
                            'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                            'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                            <!-- END PAGE LEVEL PLUGINS -->
                            'js/controllers/ProfileController.js'
                        ]
                    });
                }]
            }
        })
        .state('countries', {
            url: "/countries",
            templateUrl: "views/partials/countries.html",
            controller: "CountriesController",
            data: {pageTitle: 'Countries'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/CountriesController.js'
                        ]
                    });
                }]
            }
        })
        .state('cities', {
            url: "/cities",
            templateUrl: "views/partials/cities.html",
            controller: "CitiesController",
            data: {pageTitle: 'Cities'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/CitiesController.js'
                        ]
                    });
                }]
            }
        })
        .state('categories', {
            url: "/categories",
            templateUrl: "views/partials/categories.html",
            controller: "CategoriesController",
            data: {pageTitle: 'Categories'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/CategoriesController.js'
                        ]
                    });
                }]
            }
        })
        .state('tags', {
            url: "/tags",
            templateUrl: "views/partials/tags.html",
            controller: "TagsController",
            data: {pageTitle: 'Tags'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/TagsController.js'
                        ]
                    });
                }]
            }
        })
        .state('partners', {
            url: "/partners",
            templateUrl: "views/partials/partners.html",
            controller: "PartnersController",
            data: {pageTitle: 'Partners'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/PartnersController.js'
                        ]
                    });
                }]
            }
        })
        .state('tabs', {
            url: "/tabs",
            templateUrl: "views/partials/home-tabs.html",
            controller: "HomeTabsController",
            data: {pageTitle: 'Home Tabs'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/HomeTabsController.js'
                        ]
                    });
                }]
            }
        })
        .state('sliders', {
            url: "/sliders",
            templateUrl: "views/partials/sliders.html",
            controller: "SlidersController",
            data: {pageTitle: 'Sliders'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/SlidersController.js'
                        ]
                    });
                }]
            }
        })
        .state('rooms', {
            url: "/rooms",
            templateUrl: "views/partials/rooms.html",
            controller: "RoomsController",
            data: {pageTitle: 'Rooms'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/RoomsController.js'
                        ]
                    });
                }]
            }
        })
        .state('hotels', {
            url: "/hotels",
            templateUrl: "views/partials/hotels.html",
            controller: "HotelsController",
            data: {pageTitle: 'Hotels'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/HotelsController.js'
                        ]
                    });
                }]
            }
        })
        .state('trips', {
            url: "/trips",
            templateUrl: "views/partials/trips.html",
            controller: "TripsController",
            data: {pageTitle: 'Trips'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/TripsController.js'
                        ]
                    });
                }]
            }
        })
        .state('newTrips', {
            url: "/trips/new",
            templateUrl: "views/partials/addTrip.html",
            controller: "AddTripController",
            data: {pageTitle: 'New Trip'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/AddTripController.js'
                        ]
                    });
                }]
            }
        })
        .state('editTrips', {
            url: "/trips/:tripId/:isEditable",
            templateUrl: "views/partials/addTrip.html",
            controller: "AddTripController",
            data: {pageTitle: 'Trip'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/AddTripController.js'
                        ]
                    });
                }]
            }
        })
        .state('tripSlider', {
            url: "/trip/slider/:tripID",
            templateUrl: "views/partials/tripSlider.html",
            controller: "TripSlidersController",
            data: {pageTitle: 'Trip Slider'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/TripSlidersController.js'
                        ]
                    });
                }]
            }
        })
        .state('settings', {
            url: "/settings",
            templateUrl: "views/partials/settings.html",
            controller: "SettingsController",
            data: {pageTitle: 'Settings'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/SettingsController.js'
                        ]
                    });
                }]
            }
        })
        .state('faqs', {
            url: "/faqs",
            templateUrl: "views/partials/faqs.html",
            controller: "FAQsController",
            data: {pageTitle: 'FAQs'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/FAQsController.js'
                        ]
                    });
                }]
            }
        })
        .state('nationalities', {
            url: "/nationalities",
            templateUrl: "views/partials/nationalities.html",
            controller: "NationalityController",
            data: {pageTitle: 'Nationalities'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/NationalityController.js'
                        ]
                    });
                }]
            }
        })
        .state('tours-language', {
            url: "/tours-language",
            templateUrl: "views/partials/tours-language.html",
            controller: "ToursLanguageController",
            data: {pageTitle: 'Tours Language'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/ToursLanguageController.js'
                        ]
                    });
                }]
            }
        })
        .state('trip-reservations', {
            url: "/trip-reservations",
            templateUrl: "views/partials/trip-reservations.html",
            controller: "TripReservationsController",
            data: {pageTitle: 'Trip Reservations'},
            resolve: {
                deps: ['$ocLazyLoad', function ($ocLazyLoad) {
                    return $ocLazyLoad.load({
                        name: 'MetronicApp',
                        insertBefore: '#ng_load_plugins_before', // load the above css files before '#ng_load_plugins_before'
                        files: [
                        'assets/global/plugins/datatables/datatables.min.css',
                        'assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css',
                        'assets/global/plugins/datatables/datatables.all.min.js',
                        <!-- BEGIN PAGE LEVEL PLUGINS -->
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.css',
                        'assets/global/plugins/angularjs/plugins/ui-select/select.min.js',
                        <!-- END PAGE LEVEL PLUGINS -->
                        'js/controllers/TripReservationsController.js'
                        ]
                    });
                }]
            }
        })
                        
}]);
/* Init global settings and run the app */
MetronicApp.run(["$rootScope", "settings", "$state", "$location", "localStorageService", 
    function ($rootScope, settings, $state, $location, localStorageService) {
    $rootScope.$state = $state; // state to be accessed from view
    $rootScope.$settings = settings; // state to be accessed from view
    $rootScope.globals = {};
    $rootScope.location = $location;
    
    $rootScope.refresh = function()
    {
        $state.go($state.current,
            {},
            {
                reload: true
            });
    }
    $rootScope.checkPermission = function(parent, child) {
        if ($rootScope.permissions) {
            if (eval("$rootScope.permissions." + parent)) {
                if (!child) {
                    if (eval("$rootScope.permissions." + parent)) return true;
                } else {
                    if (eval("$rootScope.permissions." + parent + ".indexOf('" + child + "')") > -1) return true;
                }
                return false;
            }
            return false;
        }
        return false;
    }

    if (!$rootScope.globals.loggedUser && localStorageService.getData('TahrirAdmin')) 
    {
        $rootScope.globals.loggedUser = localStorageService.getData('TahrirAdmin');
        if (!$rootScope.globals.settings && localStorageService.getData('settings')) 
        {
            $rootScope.globals.settings = localStorageService.getData('settings');
        }
        $rootScope.permissions = mapper.mapPermissions($rootScope.globals.loggedUser.permissions);
    }

    $rootScope.hasGroup = function(groupName)
    {
        if ($rootScope.globals.loggedUser)
        {
            if ($rootScope.globals.loggedUser.groups)
            {
                for (var i = $rootScope.globals.loggedUser.groups.length - 1; i >= 0; i--)
                {
                    if ($rootScope.globals.loggedUser.groups[i].name == groupName)
                    {
                        return true;
                    }
                }
            }
        }
    }

    /* to check is logged user is admin */
    $rootScope.checkisAdmin = function()
    {
        $rootScope.isAdmin = $rootScope.hasGroup('Admin') ? true : false;
    }
    $rootScope.checkisAdmin();

    $rootScope.$on('$locationChangeStart', function(event, next, current)
    {
        // redirect to login page if not logged in
        if ($location.path() !== '/login' && $location.path().indexOf('resetPassword') < 0 && !$rootScope.globals.loggedUser && !localStorageService.getData('TahrirAdmin'))
        {
            $state.go('login');
            event.preventDefault();
        }

        $rootScope.loggedUser = localStorageService.getData('TahrirAdmin');
    });
    $rootScope.$on('$stateChangeStart',
        function(event, toState, toParams, fromState, fromParams)
        {
            if ($state.current.name !== 'login' && $state.current.name !== 'resetPassword' && !$rootScope.globals.loggedUser && toState.name != 'login' && toState.name != 'resetPassword' && !localStorageService.getData('TahrirAdmin'))
            {
                event.preventDefault();
                $state.go('login');
            }
        });
    }
]);
