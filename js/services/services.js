/*
 * Authentication service
 */
 angular.module("MetronicApp").factory('AuthenticationService', ['$http', '$rootScope', '$timeout', 'serverip', 'localStorageService', 'HEADERS', '$state',
    function($http, $rootScope, $timeout, serverip, localStorageService, HEADERS, $state)
    {
        var service = {};
        service.Login = function(email, password, callback)
        {
            $http.post(serverip + "acl/users/login",
            {
                email: email,
                password: password,
                admin: "1"
            },
            {
                headers: HEADERS
            }).success(function(respose, status, headers, config)
            {
                callback(respose, status);
            }).error(function(response, status)
            {
                callback(response, status);
            });
        };
        service.Logout = function(callback)
        {
            var head = HEADERS;
            if (localStorageService.getData('TahrirAdmin'))
            {
                head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
            }
            $http.get(serverip + "acl/users/logout",
            {
                headers: head
            }).success(function(respose, status, headers, config)
            {
                console.log(respose);
                callback(respose, status);
            }).error(function(response, status)
            {
                console.log(response);
                callback(response, status);
            }); 
        };
        service.SetCredentials = function(authToken)
        {
            localStorageService.setData('TahrirAdmin',
            {
                authToken: authToken
            });

            var head = HEADERS;
            head.Authorization = "bearer " + authToken;
            $rootScope.promise = $http.get(serverip + "acl/users/account",
            {
                headers: HEADERS
            });
            $rootScope.promise.then(function(response)
            {
                var userData = response.data;
                userData.authToken = "bearer " + authToken;
                $rootScope.globals = {
                    loggedUser: userData
                };
                $rootScope.permissions = mapper.mapPermissions($rootScope.globals.loggedUser.permissions);
                localStorageService.setData('TahrirAdmin', $rootScope.globals.loggedUser);
                $rootScope.loggedUser = localStorageService.getData('TahrirAdmin');
                $rootScope.checkisAdmin();
                $state.go('trips');
                
            });

        };
        service.ClearCredentials = function()
        {
            $rootScope.globals = {};
            $rootScope.isAdmin = false;
            $rootScope.permissions = {};
            $http.defaults.headers.common.Authorization = 'Basic';
            localStorageService.clearAll();
        };
        service.ClearCredentials();
        return service;
    }
    ])
 angular.module('MetronicApp').service('Dashboard', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }
    }
    ]);
 angular.module('MetronicApp').service('Users', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService,$rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin')) head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        this.listUsers = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "acl/users/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getALlUsers = function()
        {
            return $http.get(serverip + "acl/users/list",
            {
                headers: head
            });
        };
        this.addUser = function(user)
        {
            user = JSON.stringify(user);
            return $http.post(serverip + "acl/users/save", user,
            {
                headers: head
            });
        };
        this.updateProfile = function(user)
        {
            user = JSON.stringify(user);
            return $http.post(serverip + "acl/users/profile/save", user,
            {
                headers: head
            });
        };
        this.resetPassword = function(obj)
        {
            obj = JSON.stringify(obj);
            return $http.post(serverip + "acl/users/resetpassword", obj,
            {
                headers: head
            });           
        };
        this.userGroups = function()
        {
            return $http.get(serverip + "acl/groups/list",
            {
                headers: head
            });
        };

        this.deleteUser = function(id)
        {
            return $http.get(serverip + "acl/users/delete/" + id,
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "acl/users/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };

        this.blockUser   = function(id) 
        {
            return $http.get(serverip + "acl/users/block/"+id,{headers : head});
        };
        this.unblockUser = function(id) 
        {
            return $http.get(serverip + "acl/users/unblock/"+id,{headers : head});
        };

    }
    ]);
 angular.module('MetronicApp').service('Cities', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listCities = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/cities/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCities = function()
        {
            return $http.get(serverip + "tahrir/cities/list",
            {
                headers: head
            });
        };

        this.addCity = function(cities)
        {
            cities = JSON.stringify(cities);
            return $http.post(serverip + "tahrir/cities/save", cities,
            {
                headers: head
            });
        };
        
        this.deleteCity = function(id)
        {
            return $http.get(serverip + "tahrir/cities/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/cities/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };

    }
    ]);
angular.module('MetronicApp').service('Countries', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listCountries = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/countries/paginate/" + items +  '/name->en/1' + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCountries = function()
        {
            return $http.get(serverip + "tahrir/countries/list",
            {
                headers: head
            });
        };
        this.listCitiesByCountry = function(countryID) 
        {
            return $http.post(serverip + "tahrir/cities/findby",
            {
                country_id:countryID
            },
            {
                headers : head
            });
        };
        this.addCountry = function(country)
        {
            country = JSON.stringify(country);
            return $http.post(serverip + "tahrir/countries/save", country,
            {
                headers: head
            });
        };
        
        this.deleteCountry = function(id)
        {
            return $http.get(serverip + "tahrir/countries/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/countries/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Categories', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listCategories = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/categories/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllCategories = function()
        {
            return $http.get(serverip + "tahrir/categories/list",
            {
                headers: head
            });
        };
        this.addCategory = function(category)
        {
            category = JSON.stringify(category);
            return $http.post(serverip + "tahrir/categories/save", category,
            {
                headers: head
            });
        };
        
        this.deleteCategory = function(id)
        {
            return $http.get(serverip + "tahrir/categories/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/categories/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Tags', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listTags = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tags/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllTags = function()
        {
            return $http.get(serverip + "tahrir/tags/list",
            {
                headers: head
            });
        };
        this.addTag = function(tag)
        {
            tag = JSON.stringify(tag);
            return $http.post(serverip + "tahrir/tags/save", tag,
            {
                headers: head
            });
        };
        
        this.deleteTag = function(id)
        {
            return $http.get(serverip + "tahrir/tags/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tags/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Partners', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }
        this.listPartners = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/partners/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.addPartner = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/partners/save", data,
            {
                headers: head
            });
        };
        this.deletePartner = function(id)
        {
            return $http.get(serverip + "tahrir/partners/delete/" + id,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('HomeTabs', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }
        this.listTabs = function()
        {
            return $http.get(serverip + "tahrir/home/tabs/list",
            {
                headers: head
            });
        };
        this.addTab = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/home/tabs/save", data,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Sliders', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listSliders = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/slides/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllSliders = function()
        {
            return $http.get(serverip + "tahrir/slides/list",
            {
                headers: head
            });
        };
        this.addSlider = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/slides/save", data,
            {
                headers: head
            });
        };
        
        this.deleteSlider = function(id)
        {
            return $http.get(serverip + "tahrir/slides/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/slides/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Rooms', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listRooms = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/rooms/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllRooms = function()
        {
            return $http.get(serverip + "tahrir/rooms/list",
            {
                headers: head
            });
        };
        this.addRoom = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/rooms/save", data,
            {
                headers: head
            });
        };
        
        this.deleteRoom = function(id)
        {
            return $http.get(serverip + "tahrir/rooms/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/rooms/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.searchQuick = function(searchText)
        {
            return $http.get(serverip + "tahrir/rooms/search/" + searchText,
            {
                headers: head
            });
        };
        this.roomsByHotelID = function(val, hotelID) 
        {
            var condition = {};
            condition.and = 
            {
                or:
                {
                    "type->en":{"op":"like", "val":"%"+val+"%"},
                    "type->fr":{"op":"like", "val":"%"+val+"%"},
                    "type->sp":{"op":"like", "val":"%"+val+"%"}
                },
                hotels:
                {
                    "op":"has",
                    "val": {"hotels.id":hotelID}
                }
            }
            return $http.post(serverip + "tahrir/rooms/findby", condition,
            {
                headers : head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Hotels', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listHotels = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/hotels/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllHotels = function()
        {
            return $http.get(serverip + "tahrir/hotels/list",
            {
                headers: head
            });
        };
        this.addHotel = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/hotels/save", data,
            {
                headers: head
            });
        };
        
        this.deleteHotel = function(id)
        {
            return $http.get(serverip + "tahrir/hotels/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/hotels/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.searchQuick = function(searchText)
        {
            return $http.get(serverip + "tahrir/hotels/search/" + searchText,
            {
                headers: head
            });
        };
        
    }
    ]);
angular.module('MetronicApp').service('Trips', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listTrips = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/trips/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllTrips = function()
        {
            return $http.get(serverip + "tahrir/trips/list",
            {
                headers: head
            });
        };
        this.getTripByID = function(id)
        {
            return $http.get(serverip + "tahrir/trips/find/"+ id,
            {
                headers: head
            });
        };
        this.addTrip = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/trips/save", data,
            {
                headers: head
            });
        };
        
        this.deleteTrip = function(id)
        {
            return $http.get(serverip + "tahrir/trips/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/trips/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('TripReservations', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listTripReservations = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/trip/reservations/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.filterTripReservations = function(items, page, conditions)
        {
            page == undefined ? 1 : page;
            return $http.post(serverip + "tahrir/trip/reservations/paginateby/" + items + "?page=" + page, conditions,
            {
                headers: head
            });
        };
        this.getAllTripReservations = function()
        {
            return $http.get(serverip + "tahrir/trip/reservations/list",
            {
                headers: head
            });
        };
        this.getTripReservationByID = function(id)
        {
            return $http.get(serverip + "tahrir/trip/reservations/find/"+ id,
            {
                headers: head
            });
        };
        this.seen = function(id)
        {
            return $http.get(serverip + "tahrir/trip/reservations/seen/"+ id,
            {
                headers: head
            });
        };
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/trip/reservations/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('TripSlider', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listTripsImage = function(items, page, tripID)
        {
            page == undefined ? 1 : page;
            return $http.post(serverip + "tahrir/trip/images/paginateby/" + items + "?page=" + page,
            {
                trip_id:tripID
            },
            {
                headers: head
            });
        };
        this.getAllTripsImage = function()
        {
            return $http.get(serverip + "tahrir/trip/images/list",
            {
                headers: head
            });
        };
        this.addTripImage = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/trip/images/save", data,
            {
                headers: head
            });
        };
        
        this.deleteTripImage = function(id)
        {
            return $http.get(serverip + "tahrir/trip/images/delete/" + id,
            {
                headers: head
            });
        }; 
    }
    ]);
angular.module('MetronicApp').service('FAQs', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listFAQs = function(items, page, tripID)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/faqs/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllFAQs = function()
        {
            return $http.get(serverip + "tahrir/faqs/list",
            {
                headers: head
            });
        };
        this.addFaq = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "tahrir/faqs/save", data,
            {
                headers: head
            });
        };
        
        this.deleteFaq = function(id)
        {
            return $http.get(serverip + "tahrir/faqs/delete/" + id,
            {
                headers: head
            });
        }; 
        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/faqs/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Nationalities', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listNationalities = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/nationalities/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllNationalities = function()
        {
            return $http.get(serverip + "tahrir/nationalities/list",
            {
                headers: head
            });
        };
        this.addNationality = function(category)
        {
            category = JSON.stringify(category);
            return $http.post(serverip + "tahrir/nationalities/save", category,
            {
                headers: head
            });
        };
        
        this.deleteNationality = function(id)
        {
            return $http.get(serverip + "tahrir/nationalities/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/nationalities/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('ToursLanguage', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }

        this.listToursLanguage = function(items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tour_languages/paginate/" + items + "?page=" + page,
            {
                headers: head
            });
        };
        this.getAllToursLanguage = function()
        {
            return $http.get(serverip + "tahrir/tour_languages/list",
            {
                headers: head
            });
        };
        this.addToursLanguage = function(category)
        {
            category = JSON.stringify(category);
            return $http.post(serverip + "tahrir/tour_languages/save", category,
            {
                headers: head
            });
        };
        
        this.deleteToursLanguage = function(id)
        {
            return $http.get(serverip + "tahrir/tour_languages/delete/" + id,
            {
                headers: head
            });
        };

        this.search = function(searchText, items, page)
        {
            page == undefined ? 1 : page;
            return $http.get(serverip + "tahrir/tour_languages/search/" + searchText + '/' + items + "?page=" + page,
            {
                headers: head
            });
        };
    }
    ]);
angular.module('MetronicApp').service('Settings', ['$http', '$q', 'serverip', 'HEADERS', 'localStorageService','$rootScope',
    function($http, $q, serverip, HEADERS, localStorageService, $rootScope)
    {
        var head = HEADERS;
        if (localStorageService.getData('TahrirAdmin'))
        {
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        }
        this.listSettings = function()
        {
            return $http.get(serverip + "core/settings/list",
            {
                headers: head
            });
        };
        this.saveSettings = function(data)
        {
            data = JSON.stringify(data);
            return $http.post(serverip + "core/settings/save/many", data,
            {
                headers: head
            });
        };

    }
    ]);