angular.module("MetronicApp").factory('angularCachingService', function()
{
    /*
     * User setter & getters
     */
     var loggedUser = {};

     function setUser(data)
     {
        loggedUser = data;
    }

    function getUser()
    {
        return loggedUser;
    }
     return {
        setUser: setUser,
        getUser: getUser,
    }
});
angular.module("MetronicApp").factory('storageService', function($rootScope)
{
    return {
        get: function(key)
        {
            return JSON.parse(localStorage.getItem(key));
        },
        save: function(key, data)
        {
            localStorage.setItem(key, JSON.stringify(data));
        },
        remove: function(key)
        {
            localStorage.removeItem(key);
        },
        clearAll: function()
        {
            localStorage.clear();
        }
    };
});
angular.module("MetronicApp").factory('localStorageService', function($http, storageService)
{
    return {
        getData: function(key)
        {
            return storageService.get(key);
        },
        setData: function(key, data)
        {
            storageService.save(key, data);
        },
        removeData: function(key)
        {
            storageService.remove(key);
        },
        clearAll: function()
        {
            storageService.clearAll();
        }
    };
});