var time = (new Date().getTimezoneOffset() / 60) * -1;

//angular.module("MetronicApp").constant('serverip', 'http://192.168.1.15/tahrir-backend/api/v1/');

angular.module('MetronicApp').constant('serverip', 'http://51.15.132.57/tahrir/tahrir-backend/api/v1/');


/**
 * Headers for calling rest
 */
 angular.module("MetronicApp").constant('HEADERS',
 {
    'Content-Type': 'application/json',
    'Accept': 'application/json',
    'time-zone-diff': time,
    'locale':'all'
});

angular.module("MetronicApp").constant('loading_timeout', 1000);
angular.module("MetronicApp").value('debug', false); 
