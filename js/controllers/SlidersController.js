angular.module('MetronicApp').controller('SlidersController', ['$scope', '$rootScope', '$state', 'localStorageService', 'Sliders', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, Sliders, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;
        $scope.search = function(items, page)
        {
            App.startPageLoading();
            $scope.promise = Sliders.search($scope.searchText, items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                if (debug) console.log(response.data);
                $scope.page = 1;
                $scope.slidersList = response.data.data;
                $scope.pagesCount = response.data.last_page;
            }, function(error)
            {
                App.stopPageLoading();
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.onSearchButtonClicked = function(items, page)
        {
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber(items, 1);
        }
            // pagination function
            $scope.getDataWithPageNumber = function(items, page)
            {
                $scope.page = page;
            // in case of search   !angular.isUndefined() || $scope.searchText.length >0
            if ($scope.searchText)
            {
                $scope.search(items, page);
            }
            else // in case of list all
            {
                $scope.listOfSliders($scope.items, $scope.page);
            }
        } 

        $scope.listOfSliders = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = Sliders.listSliders(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.slidersList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfSliders($scope.items, $scope.page);

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addSliderModal.html',
                controller: editOrViewSliderModalCtrl,
                resolve:
                {
                    slider: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(slider)
            {
                if (debug)
                {
                    console.log('--- slider ----');
                    console.log(slider);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.openSliderModal = function()
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addSliderModal.html',
                controller: sliderModalInstanceCtrl
            });
            modalInstance.result.then(function(slider)
            {
                if (debug)
                {
                    console.log('--- slider ----');
                    console.log(slider);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.delete = function(row)
        {
            var confirm = $mdDialog.confirm()
            .title('Are you sure you would like to delete this row ?')
            .targetEvent(row)
            .ok('YES')
            .cancel('NO');
            $mdDialog.show(confirm).then(function() {
                $scope.promise = Sliders.deleteSlider(row.id);
                $scope.promise.then(function()
                {
                    $scope.slidersList.splice($scope.slidersList.indexOf(row), 1);
                }, function(error)
                {
                    if (debug) console.log(error);
                    if (!angular.isObject(error.data))
                    {
                        $scope.error = [];
                        $scope.error.push(error.data);
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                    else
                    {
                        $scope.error = error.data;
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                });
            }, function() {

            });
        }
    }
    ]);

var sliderModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Sliders, Upload, localStorageService, serverip)
{
    $scope.isEditable = true;
    $scope.slider = {};
    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.slider.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    {
        if ($scope.sliderForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = Sliders.addSlider($scope.slider);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.slider);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};
var editOrViewSliderModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, slider, Sliders, isEditable, localStorageService, Upload, serverip)
{
    $scope.slider = slider;
    $scope.isEditable  = isEditable;
    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.slider.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    { 
        if ($scope.sliderForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Sliders.addSlider($scope.slider);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.slider);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}