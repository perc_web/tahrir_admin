angular.module('MetronicApp').controller('HotelsController', ['$scope', '$rootScope', '$state', 'localStorageService', 'Hotels', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, Hotels, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;

        $scope.search = function(items, page)
        {
            App.startPageLoading();
            $scope.promise = Hotels.search($scope.searchText, items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                if (debug) console.log(response.data);
                $scope.page = 1;
                $scope.hotelsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
            }, function(error)
            {
                App.stopPageLoading();
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.onSearchButtonClicked = function(items, page)
        {
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber(items, 1);
        }
            // pagination function
            $scope.getDataWithPageNumber = function(items, page)
            {
                $scope.page = page;
            // in case of search   !angular.isUndefined() || $scope.searchText.length >0
            if ($scope.searchText)
            {
                $scope.search(items, page);
            }
            else // in case of list all
            {
                $scope.listOfHotels($scope.items, $scope.page);
            }
        } 

        $scope.listOfHotels = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = Hotels.listHotels(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.hotelsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfHotels($scope.items, $scope.page);

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addHotelModal.html',
                controller: editOrViewHotelModalCtrl,
                resolve:
                {
                    hotel: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(hotel)
            {
                if (debug)
                {
                    console.log('--- hotel ----');
                    console.log(hotel);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.openHotelModal = function()
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addHotelModal.html',
                controller: hotelModalInstanceCtrl
            });
            modalInstance.result.then(function(hotel)
            {
                if (debug)
                {
                    console.log('--- hotel ----');
                    console.log(hotel);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.delete = function(row)
        {
            var confirm = $mdDialog.confirm()
            .title('Are you sure you would like to delete this row ?')
            .targetEvent(row)
            .ok('YES')
            .cancel('NO');
            $mdDialog.show(confirm).then(function() {
                $scope.promise = Hotels.deleteHotel(row.id);
                $scope.promise.then(function()
                {
                    $scope.hotelsList.splice($scope.hotelsList.indexOf(row), 1);
                }, function(error)
                {
                    if (debug) console.log(error);
                    if (!angular.isObject(error.data))
                    {
                        $scope.error = [];
                        $scope.error.push(error.data);
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                    else
                    {
                        $scope.error = error.data;
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                });
            }, function() {

            });
        }
    }
    ]);

var hotelModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Hotels, Upload, localStorageService, serverip, Rooms, $uibModal)
{
    $scope.isEditable = true;
    $scope.hotel = {};
    $scope.hotel.rooms = [];

    $scope.openRoomModal = function()
    {
        var modalInstance = $uibModal.open(
        {
            animation: true,
            templateUrl: 'views/modals/addRoomModal.html',
            controller: roomModalInstanceCtrl
        });
        modalInstance.result.then(function(room)
        {
            if (debug)
            {
                console.log('--- room ----');
                console.log(room);
            }
        }, function()
        {
            if (debug)
            {
                console.log('---  dismiss ----');
                console.log('Modal dismissed at: ' + new Date());
            }
        });
    }
    $scope.searchRooms = function(val)
    {
        $scope.dataLoading = true
        App.startPageLoading();
        $scope.promise = Rooms.searchQuick(val);
        return $scope.promise.then(function(response)
        {
            App.stopPageLoading();
            $scope.roomsList = response.data.data;
            return $scope.roomsList;
            $timeout(function()
            {
                $scope.dataLoading = false
            }, loading_timeout);
        }, function(error)
        {
            App.stopPageLoading();
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });        
    } 
    $scope.addNewChoice = function() {
        $scope.hotel.rooms.push(angular.copy(''));
    };
    $scope.removeChoice = function(z) {
        $scope.hotel.rooms.splice(z, 1);
    };
    $scope.addNewChoice();
    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.hotel.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    {
        if ($scope.hotelForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = Hotels.addHotel($scope.hotel);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.hotel);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};
var editOrViewHotelModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, hotel, Hotels, isEditable, localStorageService, Upload, serverip, Rooms, $uibModal)
{
    $scope.hotel = hotel;
    $scope.isEditable  = isEditable;
    $scope.openroomModal = function()
    {
        var modalInstance = $uibModal.open(
        {
            animation: true,
            templateUrl: 'views/modals/addRoomModal.html',
            controller: roomModalInstanceCtrl
        });
        modalInstance.result.then(function(room)
        {
            if (debug)
            {
                console.log('--- room ----');
                console.log(room);
            }
        }, function()
        {
            if (debug)
            {
                console.log('---  dismiss ----');
                console.log('Modal dismissed at: ' + new Date());
            }
        });
    }
    $scope.searchRooms = function(val)
    {
        $scope.dataLoading = true
        App.startPageLoading();
        $scope.promise = Rooms.searchQuick(val);
        return $scope.promise.then(function(response)
        {
            App.stopPageLoading();
            $scope.roomsList = response.data.data;
            return $scope.roomsList;
            $timeout(function()
            {
                $scope.dataLoading = false
            }, loading_timeout);
        }, function(error)
        {
            App.stopPageLoading();
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });        
    } 
    $scope.addNewChoice = function() {
        $scope.hotel.rooms.push(angular.copy(''));
    };
    $scope.removeChoice = function(z) {
        $scope.hotel.rooms.splice(z, 1);
    };
    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.hotel.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    { 
        if ($scope.hotelForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Hotels.addHotel($scope.hotel);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.hotel);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}
var roomModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Rooms, localStorageService)
{
    $scope.isEditable = true;
    $scope.room = {};

    $scope.ok = function()
    {
        if ($scope.roomForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = Rooms.addRoom($scope.room);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.room);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};