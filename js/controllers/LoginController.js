angular.module('MetronicApp').controller('LoginController', ['$scope', '$rootScope', 'AuthenticationService', 
    function($scope, $rootScope, AuthenticationService)
    {

        AuthenticationService.ClearCredentials();
        // reset login status
        $scope.login = function()
        {   
            if ($scope.loginForm.$invalid) 
            {
                $scope.formInvalid = true ;
                return;
            }
            AuthenticationService.Login($scope.email, $scope.password, function(response, status)
            {
                if (status === 200)
                {
                    AuthenticationService.SetCredentials(response.token);
                }
                else if (status === 400)
                {
                    $scope.error = [];
                    $scope.error.push(response);
                }
                else
                {
                    $scope.error = [];
                    $scope.error.push(response);
                    console.log(response);
                }
            });
        };
    }
    ]);