angular.module('MetronicApp').controller('NationalityController', ['$scope', '$rootScope', '$state', 'localStorageService', 'Nationalities', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, Nationalities, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;

        $scope.search = function(items, page)
        {
            App.startPageLoading();
            $scope.promise = Nationalities.search($scope.searchText, items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                if (debug) console.log(response.data);
                $scope.page = 1;
                $scope.nationalitiesList = response.data.data;
                $scope.pagesCount = response.data.last_page;
            }, function(error)
            {
                App.stopPageLoading();
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.onSearchButtonClicked = function(items, page)
        {
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber(items, 1);
        }
            // pagination function
            $scope.getDataWithPageNumber = function(items, page)
            {
                $scope.page = page;
            // in case of search   !angular.isUndefined() || $scope.searchText.length >0
            if ($scope.searchText)
            {
                $scope.search(items, page);
            }
            else // in case of list all
            {
                $scope.listOfNationalities($scope.items, $scope.page);
            }
        } 

        $scope.listOfNationalities = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = Nationalities.listNationalities(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.nationalitiesList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfNationalities($scope.items, $scope.page);

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addNationalityModal.html',
                controller: editOrViewNationalityModalCtrl,
                resolve:
                {
                    nationality: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(nationality)
            {
                if (debug)
                {
                    console.log('--- nationality ----');
                    console.log(nationality);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.openNationalityModal = function()
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addNationalityModal.html',
                controller: nationalityModalInstanceCtrl
            });
            modalInstance.result.then(function(nationality)
            {
                if (debug)
                {
                    console.log('--- nationality ----');
                    console.log(nationality);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.delete = function(row)
        {
            var confirm = $mdDialog.confirm()
            .title('Are you sure you would like to delete this row ?')
            .targetEvent(row)
            .ok('YES')
            .cancel('NO');
            $mdDialog.show(confirm).then(function() {
                $scope.promise = Nationalities.deleteNationality(row.id);
                $scope.promise.then(function()
                {
                    $scope.nationalitiesList.splice($scope.nationalitiesList.indexOf(row), 1);
                }, function(error)
                {
                    if (debug) console.log(error);
                    if (!angular.isObject(error.data))
                    {
                        $scope.error = [];
                        $scope.error.push(error.data);
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                    else
                    {
                        $scope.error = error.data;
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                });
            }, function() {

            });
        }

    }
    ]);

var nationalityModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Nationalities)
{
    $scope.isEditable = true;
    $scope.nationality = {};

    $scope.ok = function()
    {
        if ($scope.nationalityForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = Nationalities.addNationality($scope.nationality);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.nationality);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};
var editOrViewNationalityModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, nationality, Nationalities, isEditable)
{
    $scope.nationality = nationality;
    $scope.isEditable  = isEditable;

    $scope.ok = function()
    { 
        if ($scope.nationalityForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Nationalities.addNationality($scope.nationality);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.nationality);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}