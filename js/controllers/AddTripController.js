angular.module('MetronicApp').controller('AddTripController', ['$scope', '$rootScope', '$state', '$stateParams', 'localStorageService', 'Trips', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal', '$mdDialog', 'Upload', 'serverip', 'Countries', 'Categories', 'Hotels', 'Rooms', 'Tags',
    function ($scope, $rootScope, $state, $stateParams, localStorageService, Trips, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog, Upload, serverip, Countries, Categories, Hotels, Rooms, Tags) {
        $scope.$on('$viewContentLoaded', function () {
            // initialize core components
            App.initAjax();

        });

        // initialize Data
        $scope.trip = {};
        $scope.trip.hotels = [];
        $scope.trip.prices = [];
        $scope.tripsTypes = [{
                id: "trip",
                name: "Trip"
            },
            {
                id: "accommodation",
                name: "Accommodation"
            },
            {
                id: "transfer",
                name: "Transfer"
            }
        ];

        $scope.trip.program = {};
        $scope.trip.program.en = [];
        $scope.trip.program.sp = [];
        $scope.trip.program.fr = [];
        $scope.trip.categories = [];

        $scope.uploadProgramENImg = [];
        $scope.uploadProgramFRImg = [];
        $scope.uploadProgramSPImg = [];

        $scope.isEditable = true;
        if ($stateParams.isEditable == "view") {
            $scope.isEditable = false;
        }
        $scope.listCitiesByCountryID = function (countryID) {
            App.startPageLoading();
            $scope.promise = Countries.listCitiesByCountry(countryID);
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.citiesList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.getTripByID = function (id) {
            App.startPageLoading();
            $scope.promise = Trips.getTripByID(id);
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                var dataTemp =  response.data ;
                if(dataTemp.prices.en){
                    dataTemp.prices = [] ;
                }
                dataTemp.childs_6_12 = Number(dataTemp.childs_6_12);
                dataTemp.childs_2_6 = Number(dataTemp.childs_2_6);
                $scope.trip = dataTemp;
                $scope.pickup_time = new Date();
                var hours = $scope.trip.pickup_time.substr(0, 2);
                var mints = $scope.trip.pickup_time.substr(3, 2);
                hours = parseInt(hours);
                mints = parseInt(mints);
                $scope.pickup_time.setHours(hours);
                $scope.pickup_time.setMinutes(mints);
                $scope.listCitiesByCountryID($scope.trip.country_id)
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        if ($stateParams.tripId) {
            $scope.getTripByID($stateParams.tripId);
        } else {
            $scope.newTrip = true;
        }

        $scope.back = function () {
            $state.go('trips');
        }

        $scope.resetSaveButton = function (dirUrl, index) {
            if (dirUrl == 'trip') {
                $scope.uploadTripImg = true;
            } else if (dirUrl == 'programEN') {
                $scope.uploadProgramENImg[index] = true;
            } else if (dirUrl == 'programFR') {
                $scope.uploadProgramFRImg[index] = true;
            } else if (dirUrl == 'programSP') {
                $scope.uploadProgramSPImg[index] = true;
            }
        }
        $scope.log = '';
        $scope.upload = function (file, dirUrl, index) {
            var head = {};
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
            head['Content-Type'] = undefined;
            Upload.upload({
                url: serverip + 'tahrir/media/upload/base64',
                data: {
                    image: file,
                    dir: dirUrl
                },
                headers: head
            }).then(function (resp) {
                $timeout(function () {
                    if (dirUrl == 'trip') {
                        $scope.trip.image = resp.data.path;
                        $scope.uploadTripImg = false;
                    } else if (dirUrl == 'programEN') {
                        $scope.trip.program.en[index].image = resp.data.path;
                        $scope.uploadProgramENImg[index] = false;
                    } else if (dirUrl == 'programFR') {
                        $scope.trip.program.fr[index].image = resp.data.path;
                        $scope.uploadProgramFRImg[index] = false;
                    } else if (dirUrl == 'programSP') {
                        $scope.trip.program.sp[index].image = resp.data.path;
                        $scope.uploadProgramSPImg[index] = false;
                    }

                    // if(dirUrl == 'program')
                    // {
                    //     $scope.trip.program.en[index].image = resp.data.path;  
                    //     $scope.trip.program.fr[index].image = resp.data.path;  
                    //     $scope.trip.program.sp[index].image = resp.data.path;  
                    // } 

                    $scope.log = resp.data.path + '\n' + $scope.log;

                });
            }, null, function (evt) {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.log = progressPercentage + '%';
            });
        };
        $scope.listOfCountries = function () {
            App.startPageLoading();
            $scope.promise = Countries.getAllCountries();
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.countriesList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfCountries();

        $scope.listOfCategories = function () {
            App.startPageLoading();
            $scope.promise = Categories.getAllCategories();
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.categoriesList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfCategories();

        $scope.listOfTags = function () {
            App.startPageLoading();
            $scope.promise = Tags.getAllTags();
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.tagsList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfTags();

        $scope.listOfTrips = function () {
            App.startPageLoading();
            $scope.promise = Trips.getAllTrips();
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.tripsList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false;
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfTrips();

        $scope.listOfHotels = function () {
            App.startPageLoading();
            $scope.promise = Hotels.getAllHotels();
            $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.hotelsList = response.data;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfHotels();

        $scope.searchHotels = function (val) {
            $scope.dataLoading = true
            App.startPageLoading();
            $scope.promise = Hotels.searchQuick(val);
            return $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.hotelsList = response.data.data;
                return $scope.hotelsList;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.openHotelModal = function (hotelObj) {
            var modalInstance = $uibModal.open({
                animation: true,
                templateUrl: 'views/modals/addHotelModal.html',
                controller: hotelModalInstanceCtrl,
                resolve: {
                    hotel: function () {
                        if (hotelObj) {
                            var a;
                            return angular.copy(hotelObj, a);
                        } else {
                            return false;
                        }
                    }
                }
            });
            modalInstance.result.then(function (hotel) {
                if (debug) {
                    console.log('--- hotel ----');
                    console.log(hotel);
                }
            }, function () {
                if (debug) {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }

        $scope.addNewPrice = function () {
            $scope.trip.prices.push(angular.copy(''));
        };
        $scope.removePrice = function (z) {
            $scope.trip.prices.splice(z, 1);
        };

        $scope.addNewProgram = function () {
            $scope.trip.program.en.push(angular.copy(''));
            $scope.trip.program.sp.push(angular.copy(''));
            $scope.trip.program.fr.push(angular.copy(''));
        };
        $scope.removeProgram = function (z) {
            $scope.trip.program.en.splice(z, 1);
            $scope.trip.program.sp.splice(z, 1);
            $scope.trip.program.fr.splice(z, 1);
        };
        $scope.addNewHotel = function () {
            $scope.trip.hotels.push(angular.copy(''));
        };
        $scope.removeHotel = function (z) {
            $scope.trip.hotels.splice(z, 1);
        };
        $scope.addNewRoom = function (hotel) {
            hotel.rooms.push(angular.copy(''));
        };
        $scope.removeRoom = function (hotel, z) {
            hotel.rooms.splice(z, 1);
        };
        $scope.selectHotel = function (hotel) {
            if (hotel.id) {
                hotel.rooms = [];
                $scope.addNewRoom(hotel);
            }
        }
        if ($scope.newTrip) {
            $scope.addNewPrice();
            $scope.addNewProgram();
        }
        $scope.hasAccommodation = function (has_accommodation) {
            $scope.trip.type = $scope.tripsTypes[1].id;
            if (has_accommodation && $scope.trip.hotels.length === 0) {
                $scope.addNewHotel();
            }
        }
        $scope.searchRoomsByHotelID = function (val, hotelID) {
            $scope.dataLoading = true
            App.startPageLoading();
            $scope.promise = Rooms.roomsByHotelID(val, hotelID);
            return $scope.promise.then(function (response) {
                App.stopPageLoading();
                $scope.roomsList = response.data;
                return $scope.roomsList;
                $timeout(function () {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function (error) {
                App.stopPageLoading();
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.ok = function () {
            console.log($scope.trip);
            console.log($scope.tripForm);
            if ($scope.tripForm.$invalid) {
                $scope.formInvalid = true;
                return;
            }
            if (!$scope.trip.has_accommodation) {
                $scope.trip.hotels = [];
            }
            if($scope.trip.type != 'transfer'){
                console.log("Doff");
                delete $scope.trip.drop_off;
            }

            $scope.trip.pickup_time = $filter('date')(Date.parse($scope.pickup_time), 'HH:mm');
            $scope.saving = true;
            App.startPageLoading();
            $scope.promise = Trips.addTrip($scope.trip);
            $scope.promise.then(function () {
                App.stopPageLoading();
                $scope.saving = false;
                $state.go('trips');
            }, function (error) {
                App.stopPageLoading();
                $scope.saving = false;
                if (debug) console.log(error);
                if (!angular.isObject(error.data)) {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                } else {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        };
    }
]);

var hotelModalInstanceCtrl = function ($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Hotels, Upload, localStorageService, serverip, Rooms, $uibModal, hotel) {
    $scope.isEditable = true;

    if (hotel) {
        $scope.hotel = hotel;
        $scope.hotel.rooms.pop();
        $scope.editRooms = true;
    } else {
        $scope.hotel = {};
        $scope.hotel.rooms = [];
    }

    $scope.openRoomModal = function () {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'views/modals/addRoomModal.html',
            controller: roomModalInstanceCtrl
        });
        modalInstance.result.then(function (room) {
            if (debug) {
                console.log('--- room ----');
                console.log(room);
            }
        }, function () {
            if (debug) {
                console.log('---  dismiss ----');
                console.log('Modal dismissed at: ' + new Date());
            }
        });
    }
    $scope.addNewChoice = function () {
        $scope.hotel.rooms.push(angular.copy(''));
    };
    $scope.removeChoice = function (z) {
        $scope.hotel.rooms.splice(z, 1);
    };
    $scope.addNewChoice();

    $scope.searchRooms = function (val) {
        $scope.dataLoading = true
        App.startPageLoading();
        $scope.promise = Rooms.searchQuick(val);
        return $scope.promise.then(function (response) {
            App.stopPageLoading();
            $scope.roomsList = response.data.data;
            return $scope.roomsList;
            $timeout(function () {
                $scope.dataLoading = false
            }, loading_timeout);
        }, function (error) {
            App.stopPageLoading();
            if (!angular.isObject(error.data)) {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            } else {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    }

    $scope.log = '';
    $scope.upload = function (file, dirUrl) {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload({
            url: serverip + 'tahrir/media/upload/base64',
            data: {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function (resp) {
            $timeout(function () {
                $scope.hotel.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = true;
            });
        }, null, function (evt) {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function () {
        if ($scope.hotelForm.$invalid) {
            $scope.formInvalid = true;
            return;
        }
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Hotels.addHotel($scope.hotel);
        $scope.promise.then(function () {
            App.stopPageLoading();
            $modalInstance.close($scope.hotel);
            $scope.saving = false;
        }, function (error) {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data)) {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            } else {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};
var roomModalInstanceCtrl = function ($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Rooms, localStorageService) {
    $scope.isEditable = true;
    $scope.room = {};

    $scope.ok = function () {
        if ($scope.roomForm.$invalid) {
            $scope.formInvalid = true;
            return;
        }
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Rooms.addRoom($scope.room);
        $scope.promise.then(function () {
            App.stopPageLoading();
            $modalInstance.close($scope.room);
            $scope.saving = false;
        }, function (error) {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data)) {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            } else {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };
};