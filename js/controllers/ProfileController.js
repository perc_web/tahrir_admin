angular.module('MetronicApp').controller('ProfileController', ['$scope', '$rootScope', '$state', 'localStorageService', 'debug', '$window', 'Users',
    function($scope, $rootScope, $state, localStorageService, debug, $window, Users)
    {
        $scope.user = $rootScope.globals.loggedUser;
        $scope.add = function()
        {
            if ($scope.user.password != $scope.repassword)
            {
                alert("password doesnt match");
                $scope.dataLoading = false;
                return;
            }
            $scope.saving = true;
            App.startPageLoading();
            $scope.promise = Users.updateProfile($scope.user);
            $scope.promise.then(function(data)
            {
                App.stopPageLoading();
                $scope.saving = false;
                if (debug) console.log(data.data);
                $state.go('login');
                /*
                AuthenticationService.ClearCredentials();
                AuthenticationService.SetCredentials(data.data);*/
                //$state.go("dashboard");
            }, function(error)
            {
                App.stopPageLoading();
                $scope.saving = false;
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        };
    }
    ]);