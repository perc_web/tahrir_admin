angular.module('MetronicApp').controller('LogoutController', ['$scope', '$rootScope', '$state', 'AuthenticationService', 'localStorageService',
    function($scope, $rootScope, $state, AuthenticationService, localStorageService)
    {
        // reset login status
        AuthenticationService.Logout(function(response, status)
        {
            if (status === 200)
            {
                AuthenticationService.ClearCredentials();
                $state.go('login');
            }
            else if (status === 401)
            {
                $scope.error = response;
            }
            else
            {
                console.log(response);
            }
        });
    }
    ]);