angular.module('MetronicApp').controller('TripReservationsController', ['$scope', '$rootScope', '$state', 'localStorageService', 'TripReservations', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, TripReservations, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;

        $scope.search = function(items, page, seen)
        {
            App.startPageLoading();
            $scope.promise = TripReservations.search($scope.searchText, items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                if (debug) console.log(response.data);
                $scope.page = 1;
                $scope.tripReservationsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
            }, function(error)
            {
                App.stopPageLoading();
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.onSearchButtonClicked = function(items, page)
        {
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber(items, 1);
        }
        // pagination function
        $scope.getDataWithPageNumber = function(items, page)
        {
            $scope.page = page;
            // in case of search   !angular.isUndefined() || $scope.searchText.length >0
            console.log($scope.searchText);
            if ($scope.searchText)
            {
                $scope.search(items, page);
            }
            else // in case of list all
            {
                $scope.listOfTripReservations($scope.items, $scope.page);
            }
        } 
        $scope.filter = function(seen) 
        {
            $scope.conditions     = {};
            $scope.conditions.and = {};
            if(seen == 'seen')
            {
                $scope.conditions.and.seen = 1;
            }
            else if(seen == 'unseen')
            {
                $scope.conditions.and.seen = 0;
            }
            else
            {
                delete $scope.conditions;
            }
            delete $scope.searchText;
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber($scope.items, 1);
        }

        $scope.listOfTripReservations = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = $scope.conditions ? TripReservations.filterTripReservations(items, page, $scope.conditions) : TripReservations.listTripReservations(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.tripReservationsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfTripReservations($scope.items, $scope.page);

        $scope.seen = function(row)
        {
            App.startPageLoading();
            $scope.promise = TripReservations.seen(row.id);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $rootScope.refresh();
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/tripReservationModal.html',
                controller: viewTripReservationModalCtrl,
                resolve:
                {
                    tripReservation: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                }
            });
        }
    }
]);
var viewTripReservationModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, tripReservation)
{
    $scope.tripReservation = tripReservation;
    $scope.cancel          = function()
    {
        $modalInstance.dismiss('cancel');
    };
}