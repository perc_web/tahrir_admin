angular.module('MetronicApp').controller('PartnersController', ['$scope', '$rootScope', '$state', 'localStorageService', 'Partners', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, Partners, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;

        $scope.listOfPartners = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = Partners.listPartners(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.partnersList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfPartners($scope.items, $scope.page);

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addPartnerModal.html',
                controller: editOrViewPartnerModalCtrl,
                resolve:
                {
                    partner: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(partner)
            {
                if (debug)
                {
                    console.log('--- partner ----');
                    console.log(partner);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.openPartnerModal = function()
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addPartnerModal.html',
                controller: partnerModalInstanceCtrl
            });
            modalInstance.result.then(function(partner)
            {
                if (debug)
                {
                    console.log('--- partner ----');
                    console.log(partner);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.delete = function(row)
        {
            var confirm = $mdDialog.confirm()
            .title('Are you sure you would like to delete this row ?')
            .targetEvent(row)
            .ok('YES')
            .cancel('NO');
            $mdDialog.show(confirm).then(function() {
                $scope.promise = Partners.deletePartner(row.id);
                $scope.promise.then(function()
                {
                    $scope.partnersList.splice($scope.partnersList.indexOf(row), 1);
                }, function(error)
                {
                    if (debug) console.log(error);
                    if (!angular.isObject(error.data))
                    {
                        $scope.error = [];
                        $scope.error.push(error.data);
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                    else
                    {
                        $scope.error = error.data;
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                });
            }, function() {

            });
        }

    }
    ]);

var partnerModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, Partners, Upload, localStorageService, serverip)
{
    $scope.isEditable = true;
    $scope.partner = {};

    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.partner.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    {
        if ($scope.partnerForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = Partners.addPartner($scope.partner);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.partner);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};
var editOrViewPartnerModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, partner, Partners, isEditable, localStorageService, Upload, serverip)
{
    $scope.partner = partner;
    $scope.isEditable  = isEditable;
    $scope.resetSaveButton = function()
    {
        $scope.uploadImg = true;
    }
    $scope.log = '';
    $scope.upload = function(file, dirUrl)
    {
        var head = {};
        head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
        head['Content-Type'] = undefined;
        Upload.upload(
        {
            url: serverip + 'tahrir/media/upload/base64',
            data:
            {
                image: file,
                dir: dirUrl
            },
            headers: head
        }).then(function(resp)
        {
            $timeout(function()
            {
                $scope.partner.image = resp.data.path;
                $scope.log = resp.data.path + '\n' + $scope.log;
                $scope.uploadImg = false;
            });
        }, null, function(evt)
        {
            var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
            $scope.log = progressPercentage + '%';
        });
    };
    $scope.ok = function()
    { 
        if ($scope.partnerForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = Partners.addPartner($scope.partner);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.partner);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}