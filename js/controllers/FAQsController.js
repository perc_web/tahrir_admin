angular.module('MetronicApp').controller('FAQsController', ['$scope', '$rootScope', '$state', 'localStorageService', 'FAQs', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, FAQs, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.page = 1;
        $scope.items = 10;
        $scope.pagesCount = 1;

        $scope.search = function(items, page)
        {
            App.startPageLoading();
            $scope.promise = FAQs.search($scope.searchText, items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                if (debug) console.log(response.data);
                $scope.page = 1;
                $scope.faqsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
            }, function(error)
            {
                App.stopPageLoading();
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.onSearchButtonClicked = function(items, page)
        {
            $scope.page = 1;
            $scope.items = 10;
            $scope.getDataWithPageNumber(items, 1);
        }
            // pagination function
            $scope.getDataWithPageNumber = function(items, page)
            {
                $scope.page = page;
            // in case of search   !angular.isUndefined() || $scope.searchText.length >0
            if ($scope.searchText)
            {
                $scope.search(items, page);
            }
            else // in case of list all
            {
                $scope.listOfFAQs($scope.items, $scope.page);
            }
        } 

        $scope.listOfFAQs = function(items, page)
        {
            App.startPageLoading();
            $scope.page = page;
            $scope.promise = FAQs.listFAQs(items, page);
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.faqsList = response.data.data;
                $scope.pagesCount = response.data.last_page;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfFAQs($scope.items, $scope.page);

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addFaqModal.html',
                controller: editOrViewFaqModalCtrl,
                resolve:
                {
                    faq: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(faq)
            {
                if (debug)
                {
                    console.log('--- faq ----');
                    console.log(faq);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.openFaqModal = function()
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/addFaqModal.html',
                controller: faqModalInstanceCtrl
            });
            modalInstance.result.then(function(faq)
            {
                if (debug)
                {
                    console.log('--- faq ----');
                    console.log(faq);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }
        $scope.delete = function(row)
        {
            var confirm = $mdDialog.confirm()
            .title('Are you sure you would like to delete this row ?')
            .targetEvent(row)
            .ok('YES')
            .cancel('NO');
            $mdDialog.show(confirm).then(function() {
                $scope.promise = FAQs.deleteFaq(row.id);
                $scope.promise.then(function()
                {
                    $scope.faqsList.splice($scope.faqsList.indexOf(row), 1);
                }, function(error)
                {
                    if (debug) console.log(error);
                    if (!angular.isObject(error.data))
                    {
                        $scope.error = [];
                        $scope.error.push(error.data);
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                    else
                    {
                        $scope.error = error.data;
                        $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                    }
                });
            }, function() {

            });
        }
    }
    ]);

var faqModalInstanceCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, FAQs, Upload, localStorageService, serverip)
{
    $scope.isEditable = true;
    $scope.faq = {};

    $scope.ok = function()
    {
        if ($scope.faqForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }
        $scope.saving  = true;
        App.startPageLoading();
        $scope.promise = FAQs.addFaq($scope.faq);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.faq);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };

    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
};
var editOrViewFaqModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, faq, FAQs, isEditable, localStorageService, Upload, serverip)
{
    $scope.faq = faq;
    $scope.isEditable  = isEditable;

    $scope.ok = function()
    { 
        if ($scope.faqForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = FAQs.addFaq($scope.faq);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.faq);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}