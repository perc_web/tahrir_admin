angular.module('MetronicApp').controller('HomeTabsController', ['$scope', '$rootScope', '$state', 'localStorageService', 'HomeTabs', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog',
    function($scope, $rootScope, $state, localStorageService, HomeTabs, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

        });

        $scope.listOfTabs = function()
        {
            App.startPageLoading();
            $scope.promise = HomeTabs.listTabs();
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.tabsList = response.data;
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }

        $scope.listOfTabs();

        $scope.view = function(row, isEdit)
        {
            var modalInstance = $uibModal.open(
            {
                animation: true,
                templateUrl: 'views/modals/homeTabModal.html',
                controller: editOrViewTabModalCtrl,
                resolve:
                {
                    tab: function()
                    {
                        var a;
                        return angular.copy(row, a);
                    },
                    isEditable: function()
                    {
                        if (isEdit) return true;
                        else return false;
                    }
                }
            });
            modalInstance.result.then(function(tab)
            {
                if (debug)
                {
                    console.log('--- tab ----');
                    console.log(tab);
                }
                $rootScope.refresh();
            }, function()
            {
                if (debug)
                {
                    console.log('---  dismiss ----');
                    console.log('Modal dismissed at: ' + new Date());
                }
            });
        }

    }
    ]);

var editOrViewTabModalCtrl = function($scope, $rootScope, $filter, $modalInstance, $timeout, loading_timeout, debug, $window, tab, HomeTabs, isEditable, localStorageService, serverip)
{
    $scope.tab = tab;
    $scope.isEditable  = isEditable;
    
    $scope.ok = function()
    { 
        if ($scope.tabsForm.$invalid) 
        {
            $scope.formInvalid = true ;
            return;
        }        
        $scope.saving = true;
        App.startPageLoading();
        $scope.promise = HomeTabs.addTab($scope.tab);
        $scope.promise.then(function()
        {
            App.stopPageLoading();
            $modalInstance.close($scope.tab);
            $scope.saving = false;
        }, function(error)
        {
            App.stopPageLoading();
            $scope.saving = false;
            if (debug) console.log(error);
            if (!angular.isObject(error.data))
            {
                $scope.error = [];
                $scope.error.push(error.data);
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
            else
            {
                $scope.error = error.data;
                $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
            }
        });
    };
    $scope.cancel = function()
    {
        $modalInstance.dismiss('cancel');
    };
}