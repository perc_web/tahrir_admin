angular.module('MetronicApp').controller('SettingsController', ['$scope', '$rootScope', '$state', 'localStorageService', 'Settings', '$timeout', 'loading_timeout', '$filter', '$window', 'debug', '$uibModal','$mdDialog','Upload','serverip',
    function($scope, $rootScope, $state, localStorageService, Settings, $timeout, loading_timeout, $filter, $window, debug, $uibModal, $mdDialog, Upload, serverip)
    {
        $scope.$on('$viewContentLoaded', function () {
        // initialize core components
        App.initAjax();

    });

        $scope.addNewPhone = function() {
            $scope.setting.telephones.value.push(angular.copy(''));
        };
        $scope.removePhone = function(z) {
            $scope.setting.telephones.value.splice(z, 1);
        };
        $scope.addNewPackageImage = function() {
            $scope.setting.package_page_images.value.push(angular.copy(''));
        };
        $scope.removePackageImage = function(z) {
            $scope.setting.package_page_images.value.splice(z, 1);
        };
        $scope.addNewContactEmail = function() {
            $scope.setting.contact_emails.value.push(angular.copy(''));
        };
        $scope.removeContactEmail = function(z) {
            $scope.setting.contact_emails.value.splice(z, 1);
        };
        $scope.addNewEnquireEmail = function() {
            $scope.setting.enquiry_emails.value.push(angular.copy(''));
        };
        $scope.removeEnquireEmail = function(z) {
            $scope.setting.enquiry_emails.value.splice(z, 1);
        };
        $scope.listOfSettings = function()
        {
            App.startPageLoading();
            $scope.promise = Settings.listSettings();
            $scope.promise.then(function(response)
            {
                App.stopPageLoading();
                $scope.setting = response.data;
                if($scope.setting.telephones.value.length < 1) $scope.addNewPhone();
                if($scope.setting.package_page_images.value.length < 1) $scope.addNewPackageImage();
                if($scope.setting.contact_emails.value.length < 1) $scope.addNewContactEmail();
                // if($scope.setting.enquiry_emails.value.length < 1) $scope.addNewEnquireEmail();
                $timeout(function()
                {
                    $scope.dataLoading = false
                }, loading_timeout);
            }, function(error)
            {
                App.stopPageLoading();
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        }
        $scope.listOfSettings();

        $scope.uploadPackageImg = [];
        $scope.resetSaveButton = function(dirUrl, index)
        {
            if(dirUrl == 'FAQs')
            {
                $scope.uploadFAQsImg = true; 
            } 
            else if(dirUrl == 'Contact') 
            {
                $scope.uploadContactImg= true; 
            }  
            else if(dirUrl == 'Privacy')
            {
                $scope.uploadPrivacyImg = true;    
            } 
            else if(dirUrl == 'Terms')
            {
                $scope.uploadTermsImg = true;   
            } 
            else if(dirUrl == 'Packages')
            {
                $scope.uploadPackageImg[index] = true; 
            } 
        }
        $scope.log = '';
        $scope.upload = function(file, dirUrl, index)
        {
            var head = {};
            head.Authorization = localStorageService.getData('TahrirAdmin').authToken;
            head['Content-Type'] = undefined;
            Upload.upload(
            {
                url: serverip + 'tahrir/media/upload/base64',
                data:
                {
                    image: file,
                    dir: dirUrl
                },
                headers: head
            }).then(function(resp)
            {
                $timeout(function()
                {
                    if(dirUrl == 'FAQs')
                    {
                        $scope.uploadFAQsImg = false; 
                        $scope.setting.faqs_page.value.image = resp.data.path;
                    } 
                    else if(dirUrl == 'Contact') 
                    {
                        $scope.uploadContactImg = false; 
                        $scope.setting.contact_page.value.image = resp.data.path;
                    }  
                    else if(dirUrl == 'Privacy')
                    {
                        $scope.uploadPrivacyImg = false;
                        $scope.setting.privacy_page.value.image = resp.data.path;
                    } 
                    else if(dirUrl == 'Terms')
                    {
                        $scope.uploadTermsImg = false;
                        $scope.setting.terms_page.value.image = resp.data.path;
                    } 
                    else if(dirUrl == 'Packages')
                    {
                        $scope.uploadPackageImg[index] = false; 
                        $scope.setting.package_page_images.value[index].image = resp.data.path;
                    }
                    $scope.log = resp.data.path + '\n' + $scope.log;

                });
            }, null, function(evt)
            {
                var progressPercentage = parseInt(100.0 * evt.loaded / evt.total);
                $scope.log = progressPercentage + '%';
            });
        };

        $scope.ok = function()
        {
            $scope.success = false;
            console.log($scope.setting);
            if ($scope.settingForm.$invalid) 
            {
                $scope.formInvalid = true ;
                return;
            }
            $scope.saving  = true;
            App.startPageLoading();
            $scope.promise = Settings.saveSettings($scope.setting);
            $scope.promise.then(function()
            {
                console.log('save')
                App.stopPageLoading();
                $scope.saving = false;
                $scope.success = true;
                $window.scrollTo(0, angular.element(document.getElementById('success')).offsetTop);
                    
            }, function(error)
            {
                App.stopPageLoading();
                $scope.saving = false;
                if (debug) console.log(error);
                if (!angular.isObject(error.data))
                {
                    $scope.error = [];
                    $scope.error.push(error.data);
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
                else
                {
                    $scope.error = error.data;
                    $window.scrollTo(0, angular.element(document.getElementById('error')).offsetTop);
                }
            });
        };

    }
    ]);