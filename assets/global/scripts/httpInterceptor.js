(function(){
	var httpInterceptor = function ($provide, $httpProvider) {
		$provide.factory('httpInterceptor', function ($q) {
			return {
				response: function (response) {
					return response || $q.when(response);
				},
				responseError: function (rejection) {
					if(rejection.status === 403) 
					{
						window.location = "./#/login";
						event.preventDefault();
					}
					
					return $q.reject(rejection);
				}
			};
		});
		$httpProvider.interceptors.push('httpInterceptor');
	};
	angular.module('MetronicApp').config(httpInterceptor);
}());